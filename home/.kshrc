#!/bin/sh
set -o vi
alias vs="xbps-query -Rs"
alias vsl="xbps-query -s"
alias vu="doas xbps-install -Su"
alias vq="xbps-query -R"
alias vr="doas xbps-remove"
alias vid="arcan-wayland -exec-x11 mpv"
alias git="git -C $HOME/downloads/software"
alias sxiv="nsxiv"

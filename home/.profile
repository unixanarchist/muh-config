#!/bin/sh
export PATH=$PATH:$HOME/scripts
export PATH=$PATH:$HOME/bin
export PATH=$PATH:$HOME/.local/bin
export ENV=$HOME/.kshrc
#some default programs
export HISTFILE=$HOME/.kshhist
export EDITOR=nvim
export BROWSER=palemoon
export XBPS_DISTDIR=$HOME/downloads/software/void-packages
export WINEPREFIX=$HOME/games
#network
#export SOCKS_PROXY="socks5://127.0.0.1:9050"
#export GIT_DIR=$HOME/downloads/software
#themes
export XDG_RUNTIME_DIR=/tmp/runtime-deep
export QT_QPA_PLATFORMTHEME=qt5ct
export GTK_THEME=NumixSolarizedDarkBlue
# exec fbterm mtm

call plug#begin('~/.vim/plugged')
	Plug 'ziglang/zig.vim'
	Plug 'vimwiki/vimwiki'
call plug#end()
let wiki_1 = {}
let wiki_1.path = '~/projects/guides'
let wiki_1.html_template = '~/public_html/template.tpl'
let wiki_1.nested_syntaxes = {'python': 'python', 'c++': 'cpp'}
let wiki_2 = {}
let wiki_2.path = '~/projects/ziggylinux/wiki'
let wiki_2.index = 'main'
let wiki_3 = {}
let wiki_3.path = '~/projects/website'
let wiki_3.index = 'index'
let g:vimwiki_list = [wiki_3, wiki_2, wiki_1]

#!/bin/sh
sudo xbps-install -Su neovim w3m loksh opendoas iwd ngetty libressl acpilight pandoc acpi yt-dlp libchardet tmux pkg-config git wget curl bison schedtool flex gcc-ada libmpc libmpc-devel bc pipe-viewer python3-readability-lxml lz4 mksh gpgme biosdisk make aria2 pipewire openresolv xdg-user-dirs rtkit mupdf
chsh -s /bin/loksh
sudo xbps-install gpm fbterm fbv scrotty gpm-devel fbpdf 
sudo xbps-install xorg wmii mpv qutebrowser keepassxc freetype-devel harfbuzz harfbuzz-devel libXft-devel xorg-server-devel gajim gajim-omemo qt5ct maim monero-gui nsxiv
sudo xbps-install intel-video-accel intel-gpu-tools
sudo xbps-install amdvlk 
sudo xbps-install openmw wine wine-mono wine-gecko winetricks wine-tools wine-common libwine Vulkan-Headers Vulkan-Tools Vulkan-ValidationLayers vkd3d vkd3d-devel zenity 
sudo xbps-install -S wine-32bit wine-devel-32bit
sudo cp home/* ~/
sudo cp etc/* /etc
echo setting up my lazy directory structure
mkdir -p ~/downloads/software ~/media/pictures ~/media/music ~/media/books ~/desktop 
xdg-user-dirs-update
echo setting up st
git clone https://github.com/lukesmithxyz/st ~/downloads/software/st
make -C ~/downloads/software/st/
doas make -C ~/downloads/software/st/ install
echo setting up doas
sudo sed -i s/deep/$USER/ /etc/doas.conf
doas xbps-remove base-system sudo
doas ln -s /usr/bin/doas /usr/bin/sudo
echo setting up iwd
doas ln -s /etc/sv/iwd /var/service
doas rm /var/service/wpa_supplicant
doas xbps-remove wpa_supplicant
echo setting up yt-dlp
doas ln -s /usr/bin/yt-dlp /usr/bin/youtube-dl
echo setting up st
git clone https://github.com/lukesmithxyz/st
make -C ~/st/
echo now enable ngetty. Expect everything to crash. You need to manage logging in fast enough to disable agetty. Good luck.
echo You can also setup your wifi with iwtcl. --help might help. That's it.

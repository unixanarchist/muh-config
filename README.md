# muhconfig

My config files. If you want to install them (overwriting your own) follow the steps below.
```
git clone https://gitea.com/unixanarchist/muhconfig 
cd muhconfig
chmod +x postinst.sh
./postinst.sh
```
